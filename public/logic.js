function saveLocalStorage() {
    localStorage.setItem("fname", $("#fname").val());
}

function loadLocalStorage() {
    if (localStorage.getItem("fname") !== null)
        $("#fname").val(localStorage.getItem("fname"));
}
function clear() {
    localStorage.clear()
    $("#fname").val("");
}

$(document).ready(function() {
    loadLocalStorage();
    $("#form").submit(function(e) {
        e.preventDefault();
        let data =  $(this).serialize();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "https://formcarry.com/s/ztszNh-1Ms",
            data: data,
            success: function(response){
                if(response.status == "success"){
                    alert("Спасибо за сообщение!");
                    clear();
                } else {
                    alert("Произошла ошибка: " + response.message);
                }
            }
        });
    });

})
